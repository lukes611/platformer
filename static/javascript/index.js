

var g = new Game('canv');

function pause(){
  g.pause();
}

function start(){
  g.start();
}

function resume(){
  g.resume();
}

function restart(){
  g.reset();
}

function loadLevel(level, cb){
  g.loadLevel(level);
  cb();
}

$(document).ready(function(){




  document.addEventListener('keydown', function(event){
    if(event.key == 'j') g.jump();
  }, false);


  $.get('maps/1.json', (data) => {
    loadLevel(data, function(){
        var paintTimesPerSecond = 25;
        var integrationsPerSecond = 30;
        var paintMilliseconds = Math.floor(1000 / paintTimesPerSecond);
        var integrationMilliseconds = Math.floor(1000 / integrationsPerSecond);
        var integrationTimes = 8;

        setInterval(function(){
          //paint:
          g.draw();
        }, paintMilliseconds);
        setInterval(() => {
          var dt = integrationMilliseconds / integrationTimes;

          //Game.integrate(dt, integrationTimes);
          g.step(dt, integrationTimes);

        }, integrationMilliseconds);



      });
    });




});
