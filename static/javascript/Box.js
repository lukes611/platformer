'use strict';

//A 2D-Box class
function Box(x, y, width, height){
  this.p = new LV2(x,y);
  this.w = width;
  this.h = height;
}

//returns a deep copy of the box
Box.prototype.copy = function(){
    return new Box(this.p.x, this.p.y, this.w, this.h);
};

//returns true if box b2 intersects with this box
Box.prototype.intersects = function(b2){
  if(this.p.x + this.w < b2.p.x) return false;
  if(this.p.y + this.h < b2.p.y) return false;
  if(this.p.x > b2.p.x + b2.w) return false;
  if(this.p.y > b2.p.y + b2.h) return false;
  return true;
};

Box.prototype.intersectsList = function(obstacles){
  for(var i = 0; i < obstacles.length; i++)
    if(this.intersects(obstacles[i])) return i;
  return -1;
};
