
//Game object:
/*
player,
obstacles,
mob,
coins,
level_data,
level-bounding box
*/
function Game(canvasId){
  this.canvas = new LCanvas($('#' + canvasId)[0]);
  this._yDiff = 100;
  this.reset();
}

Game.prototype.reset = function(){
  this.player = new Player(50,50);
  this.changeStates('start');
  this.levelBox = new Box(0,0, 100, 100);
  this.obstacles = [];
  this.mob = [];
  this.coins = [];
  if(this._level) this.loadLevel(this._level);
};

Game.prototype.pause = function(){
  this.changeStates('pause');
};

Game.prototype.start = function(){
  this.changeStates('run');
};

Game.prototype.resume = function(){
  this.changeStates('run');
};

Game.prototype.changeStates = function(newState){
  this.gameState = newState;
  if(this.gameState == 'start'){
    $('#screen-blur').show();
    $('#pause-button').hide();
    $('#menu-container').hide();
    $('#start-button').show();
  }
  if(this.gameState == 'run'){
    $('#screen-blur').hide();
    $('#pause-button').show();
    $('#menu-container').hide();
    $('#start-button').hide();
  }
  if(this.gameState == 'pause'){
    $('#screen-blur').show();
    $('#pause-button').hide();
    $('#menu-container').show();
    $('#start-button').hide();
  }
};

Game.prototype.loadLevel = function(level){
  this._level = level;
  this.levelBox = new Box(level.window.x, level.window.y, level.window.w, level.window.h);
  this.player = new Player(level.startingPoint.x, level.startingPoint.y);
  this.coins = level.coins.map(c => {
    return {p : new LV2(c.x, c.y), r : 10, collected : false};
  });
  this.obstacles = level.obstacles.map(o => {
    return new Box(o.x, o.y, o.w, o.h);
  });
  this.mob = level.mob.map(o => new GroundMob(o.position.x, o.position.y, 19, 39, o.direction));
};

Game.prototype.toWorldCoordinates = function(){
  this.canvas.context.save();
  this.canvas.context.translate(this.canvas.w/2 - this.player.box.p.x, this.canvas.h/2 - this.player.box.p.y + this._yDiff);
};

Game.prototype.toCanvasCoordinates = function(){
  this.canvas.context.restore();
};


Game.prototype.draw = function(){
  //clear the screen
  this.canvas.setFill('rgb(31, 45, 58)');
  this.canvas.rectf(0, 0, this.canvas.w, this.canvas.h);

  //translate to the correct location
  this.toWorldCoordinates();


  //draw game window-background
  this.canvas.setStroke('rgb(236,240,241)');
  this.canvas.rect(this.levelBox.p.x,this.levelBox.p.y, this.levelBox.w, this.levelBox.h);



  //draw obstacles
  this.canvas.setFill('rgb(44,62,80)');
  this.obstacles.forEach( (b) => {this.canvas.rectf(b.p.x, b.p.y, b.w, b.h)});

  //draw coins
  this.canvas.setFill('rgb(241,196,15)');
  this.coins.forEach((c) => this.canvas.circlef(c.p.x, c.p.y, 10));

  //draw player
  this.canvas.setFill('rgb(46,204,113)');
  this.canvas.rectf(this.player.box.p.x, this.player.box.p.y, this.player.box.w, this.player.box.h);

  //draw mob
  this.canvas.setFill('rgb(192,57,43)');
  this.mob.forEach( (e) => this.canvas.rectf(e.box.p.x, e.box.p.y, e.box.w, e.box.h));

  this.toCanvasCoordinates();
};

Game.prototype.step = function(dt, count){
  if(this.gameState != 'run') return;
  for(var i = 0; i < count; i++) this.player.step(this.obstacles, dt), this.mob.forEach(m => m.step(this.obstacles, dt));
  var pb = this.player.box, gb = this.levelBox.copy();
  if(!pb.intersects(gb)) this.player = new Player(this._level.startingPoint.x, this._level.startingPoint.y);
};

Game.prototype.jump = function(){
  this.player.jump(this.obstacles);
};
