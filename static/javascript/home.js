
$(document).ready(function(){
  var g = new Game('homeCanvas');
  g.changeStates('run');
  g._yDiff = 0;

  $.get('maps/1.json', (data) => {
    g.loadLevel(data);
    var paintTimesPerSecond = 25;
    var integrationsPerSecond = 30;
    var paintMilliseconds = Math.floor(1000 / paintTimesPerSecond);
    var integrationMilliseconds = Math.floor(1000 / integrationsPerSecond);
    var integrationTimes = 8;

    setInterval(function(){
      //paint:
      g.draw();
    }, paintMilliseconds);
    setInterval(() => {
      var dt = integrationMilliseconds / integrationTimes;

      //Game.integrate(dt, integrationTimes);
      g.step(dt, integrationTimes);

    }, integrationMilliseconds);
  });
});
