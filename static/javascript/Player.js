/*
player :
  gravity [x]
  moving fwd [x]
  jumping -> jump when connected to floor, or during mini-jump [x]
  mini-jump / boost up -> occurs when will connect with right wall and
    top is only a little higher than the player bottom [x]
  when connected to right wall but not connected to bottom floor, slow slide down
    when jumping in this state, invert x-velocity
      when velocity is inverted, can connect to

*/

/*common:
connected, intersects
integrateT
*/


//Player object
function Player(x,y){
    this.box = new Box(x,y, 20, 38);
    this.state = 'normal';
    this.acc = new LV2(0, 0.0098);
    this.vel = new LV2(0.1, 0);
}

//returns whether the box is connected (adjacent) to an obstacle
//-1 if not, else the index in obstacles
//x,y are numbers in which the player is moved prior to testing intersection
//x,y can be though of as being integrated into the box prior to testing for intersection
Player.connected = function(box, obstacles, x, y){
  box.p.x += x;
  box.p.y += y;
  var intersects = box.intersectsList(obstacles);
  box.p.x -= x;
  box.p.y -= y;
  return intersects;
};


Player.prototype.resetState = function(){
  this.state = 'normal';
  this.vel.x = 0.1;
  this.acc.y = 0.00098;
};

//perform one game step, pass in obstacles (list of boxes)
//also input dt - time step
Player.prototype.step = function(obstacles, dt){

  var iy = this.integrateT('y', obstacles, dt, true);
  var ix = this.integrateT('x', obstacles, dt, false);
  var dist;
  var connectedY = Player.connected(this.box,obstacles, 0.0, 0.4) != -1;
  //console.log(this.state, iy);
  //if on the gound,
  //or was sliding down the wall but are no longer sliding down the wall
  if(connectedY || (this.state == 'slow-fall-fwd' && ix == -1)){
    this.resetState();
  }

  if(ix != -1 && (this.state == 'normal' || this.state == 'jump')){
    dist = Math.abs(obstacles[ix].p.y - (this.box.p.y + this.box.h));
    if(dist < 20.0){
        this.vel.y = -.23;
        this.state = 'mini-jump';
        this.acc.y = 0.00098;
    }
  }
  //document.getElementById('debug').innerHTML = `${this.state}`;
  //was this :
  //can slow fall:
  //var tmp_canSlowFall = this.state == 'jump';
  if(ix != -1 && !connectedY  && this.box.p.y > obstacles[ix].p.y && this.vel.y > 0 && (this.state == 'jump' || (this.state == 'normal' && !connectedY))){
    this.state = 'slow-fall-fwd';
    this.acc.y = 0;
    this.vel.y = 0.05;
  }

};

//integrate x or y
Player.prototype.integrateT = function(t, obstacles, dt, setZeroPostCol){
  this.box.p[t] += this.vel[t] * dt;
  var intersects = this.box.intersectsList(obstacles);
  if(intersects != -1){
    this.box.p[t] -= this.vel[t] * dt;
    if(setZeroPostCol) this.vel[t] = 0;
  }else this.vel[t] += this.acc[t] * dt;
  return intersects;
};

//perform a jump action
Player.prototype.jump = function(obstacles){
  var jumpInc = -0.45; //was .38
  var connectedY = Player.connected(this.box,obstacles, 0.0, 0.4) != -1;
  var connectedX = Player.connected(this.box, obstacles, 0.4, 0.0) != -1;
  if((connectedY && this.state == 'normal') || this.state == 'mini-jump'){
    this.vel.y = jumpInc;
    this.state = 'jump';
    return;
  }else if(this.state == 'slow-fall-fwd' || (connectedX && this.state == 'jump')){
    this.vel.y = jumpInc;
    this.state = 'jump';
    this.vel.x = -this.vel.x;
    this.acc.y = 0.00098;
  }

};



function GroundMob(x,y,w,h, dir){
  this.box = new Box(x,y, w, h);
  this.acc = new LV2(0, 0.0098);
  this.vel = new LV2(0.05 * ((dir == 'left') ? -1: 1), 0);
}

GroundMob.prototype.integrateT = function(t, obstacles, dt, setZeroPostCol){
  this.box.p[t] += this.vel[t] * dt;
  var intersects = this.box.intersectsList(obstacles);
  if(intersects != -1){
    this.box.p[t] -= this.vel[t] * dt;
    if(setZeroPostCol) this.vel[t] = 0;
  }else this.vel[t] += this.acc[t] * dt;
  return intersects;
};

GroundMob.prototype.step = function(obstacles, dt){
  var iy = this.integrateT('y', obstacles, dt, true);
  var ix = this.integrateT('x', obstacles, dt, false);
  if(ix != -1) this.vel.x *= -1;
};
