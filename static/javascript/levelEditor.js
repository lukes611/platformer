

//performs f for each tool button...
var forEachToolButton = function(f){
  var buttons = $('.toolButton');
  for(var i = 0; i < buttons.length; i++){
    f($(buttons[i]), buttons[i]);
  }
};


$(document).ready(function(){
  var le = new LE('editCanvas');
  //device helps with event functions
  var dev = new LDevice();
  var toolButtons = []; forEachToolButton((b,e) => toolButtons.push(b)); //a list of the tool buttons

  function paint(){
    //clear screen:
    le.clearScreen();
    //in level space
    le.drawObjects();
    //draw the cursor
    le.drawCursor();
  }

  le.loadIcons(function(){
    paint();
    setInterval(function(){
      paint();
    }, 30);
  });

  $.get('maps/3.json', (d) => {
    //le.fromJSON(d);

  });


  //clicking canvas
  dev.mouse($('#editCanvas')[0], (function(info){
    le.mouse = new LV2(info.x, info.y);

    var gridPosition = le.getMouseWorldGridCoordinate();
    var levelPosition = le.getMouseWorldCoodinate();

    if(info.type == 'start'){
      this.start = le.mouse.copy();
      le.addObject(gridPosition.copy());

    }else if(this.start != null){
      var vector = this.start.sub(le.mouse);
      if(le.currentTool == 'move') le.origin.iadd(vector);
      this.start = le.mouse.copy();

      le.addObject(gridPosition, ['obstacles', 'erase', 'coins']);

      if(info.type == 'end') this.start = null;
    }
    //console.log(info);
  }).bind({start : null}));
  $('#editCanvas').mouseout(function(){
    le.mouse = null;
  });

  function changeTool(tool, element){
    toolButtons.forEach(b => b.css({'background-color':'rgb(31, 45, 58)'}));
    element.css({'background-color':'rgb(52,73,94)'});
    le.currentTool = tool;
  };

  toolButtons.forEach(function(e,e2){
    e.click(changeTool.bind(null, e.html(), e));
    if(e.html() == le.currentTool) changeTool(le.currentTool, e);
  });


  $('#toJSONButton').click(() => {

    $('#jsonOut').val(JSON.stringify(le.toJSON(), null, 2));
  });

});
