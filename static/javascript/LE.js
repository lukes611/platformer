
//A Level Editting Object
function LE(idName){
  this.mouse = null; //the location of the current mouse (in canvas coordinates)
  this.currentTool = 'move'; //the current tool
  this.canvas = new LCanvas($('#' + idName)[0]);
  this.origin = new LV2(0,0);
  this.obstacles = {};
  this.coins = {};
  this.mobs = {};
  this.startLocation = new LV2(20,-40);
  this.finishLocation = new LV2(60,-40);
  this.checkPoints = {};

  this.toolIcons = { move : null, edit : null, erase : null, checkpoint : null };
}

//a functions to load the icons, cb is the call-back,
//this function may be called multiple times
LE.prototype.loadIcons =  function(cb){
  var toolList = ['move', 'edit', 'erase', 'checkpoint'];
  var sizes = {
    move : {w:30,h:30},
    edit : {w:40,h:40},
    erase : {w:30,h:30},
    checkpoint : {w:20,h:40}
  };

  var f = () =>{
    if(toolList.length <= 0){
      cb(this);
      return;
    }
    var item = toolList.shift();
    if(this.toolIcons[item] == null){
      var s = sizes[item];
      LCanvas.image('res/' + item + '_icon.png', s.w, s.h, icon => {
        this.toolIcons[item] = icon;
        f();
      });
    }f();
  };

  f();

};

LE.prototype.toJSON = function(){
  var st = LE.stats(this.obstacles, [this.coins, this.mobs, {b:this.startLocation, a:this.finishLocation}, this.checkPoints]);
  return {
    name : "tmp",
    author : "luke",
    window : {x : st.min.x - 100, y : st.min.y - 100, w : (st.max.x + 100) - (st.min.x-100), h : st.max.y + 100 - (st.min.y-100)},
    startingPoint : this.startLocation.sub(new LV2(0,1)).toJSON(),
    obstacles : LE.decompose(LE.deepCopyObject(this.obstacles)).map(b => { return {x:b.p.x,y:b.p.y,w:b.w,h:b.h}}),
    coins : LE.objListToArray(this.coins).map(p => {return {x : p.x+10, y : p.y+10, type : null}}),
    mob : LE.objListToArray(this.mobs).map(p => {return {position:{x : p.x, y : p.y}, type : "ground", direction : "left"}})
  };
};

LE.prototype.fromJSON = function(level){
  this.startLocation = new LV2(level.startingPoint.x, level.startingPoint.y);
  this.coins = {};
  this.mobs = {};
  this.obstacles = {};
  level.coins.forEach(c => this.addCoin(new LV2(c.x-10, c.y-10)));
  level.mob.forEach(mob => this.addMob(new LV2(mob.position.x, mob.position.y)));
  level.obstacles.forEach(o => {
    for(var y = o.y; y < o.y+o.h; y+=20){
      for(var x = o.x; x < o.x+o.w; x+=20){
        var p = new LV2(x,y);
        this.addObstacle(p);
      }
    }
  });
};


//erases items on the particular spot on the grid
LE.prototype.erase = function(location){ // a function to remove objects on a particular grid spot
  var n, x;
  var above = location.add(new LV2(0, -20));
  var aboveabove = new LV2(location.x, location.y-40);
  LE.removeFromList(this.checkPoints,aboveabove);
  LE.removeFromList(this.obstacles, location);
  LE.removeFromList(this.coins, location);
  LE.removeFromList(this.mobs, location);
  LE.removeFromList(this.mobs, above);
};

//a function to add obstacles
LE.prototype.addObstacle = function(pos){
  this.erase(pos);
  this.obstacles[pos + ''] = new Box(pos.x, pos.y, 20, 20);
};

//a function to add coins
LE.prototype.addCoin = function(pos){
  this.erase(pos);
  this.coins[pos + ''] = pos.copy();
};

//a function to add a mob
LE.prototype.addMob = function(pos){
  this.erase(pos);
  this.erase(new LV2(pos.x, pos.y + 20));
  this.mobs[pos + ''] = pos.copy();
};

//returns whether to draw the tool on the canvas in place of the pointer
LE.prototype.canDrawTool = function(toolName){
  return this.currentTool == toolName && this.hasMouse();
};

//draw all of the objects and the grid
LE.prototype.drawObjects = function(){
  this.toWorldCoodinates();
    LE.drawFigure(this.canvas, this.startLocation, 'rgb(46,204,113)');
    LE.drawFigure(this.canvas, this.finishLocation, 'rgb(243,156,18)');
    LE.drawObstacles(this.canvas, this.obstacles);
    LE.drawCoins(this.canvas, this.coins);
    LE.drawIcons(this.canvas, this.checkPoints, this.toolIcons.checkpoint);
    LE.drawMobs(this.canvas, this.mobs);
    LE.drawGrid(this.canvas, this.origin, 'white');
  this.toCanvasCoordinates();
};

//a function to check if a checkpoint can be added
LE.prototype.canAddCheckpoint = function(pos){
  var p2 = pos.copy(); p2.y += 20;
  var p3 = pos.copy(); p3.y += 40;
  if(this.coins[pos+''] !== undefined || this.coins[p2+''] !== undefined) return false;
  if(this.obstacles[pos+''] !== undefined || this.obstacles[p2+''] !== undefined) return false;
  if(this.obstacles[p3+''] === undefined) return false;
  return true;
};

//a function to add a check-point
LE.prototype.addCheckPoint = function(pos){
  if(this.canAddCheckpoint(pos)) this.checkPoints[pos + ''] = pos.copy();
};

//returns true if the mouse is in range of the canvas
LE.prototype.hasMouse = function(){
  return this.mouse !== null;
};

//returns the world coordinates for the mouse
LE.prototype.getMouseWorldCoodinate = function(){
  if(!this.hasMouse()) return null;
  return new LV2(this.mouse.x + this.origin.x - this.canvas.w/2, this.mouse.y + this.origin.y - this.canvas.h/2);
};

//returns the world-grid coordinate for the mouse
LE.prototype.getMouseWorldGridCoordinate = function(){
  var p = this.getMouseWorldCoodinate();
  if(p == null) return null;
  p.iscale(1/20); p.ifloor();
  p.iscale(20); return p;
};

//functions to convert to world and canvas coordinates
LE.prototype.toWorldCoodinates = function(){
  this.canvas.context.save(); this.canvas.context.translate(-this.origin.x + this.canvas.w/2,this.canvas.h/2 - this.origin.y);
};
LE.prototype.toCanvasCoordinates  = function(){
  this.canvas.context.restore();
};


//clear the canvas screeen
LE.prototype.clearScreen = function(){
  this.canvas.setFill('black');
  this.canvas.rectf(0,0,this.canvas.w,this.canvas.h);
};


//mouse interacting buttons
LE.prototype.addObject = function(pos, allowed){
  allowed = allowed || ['obstacles', 'erase', 'coins', 'mob', 'checkpnt', 'start', 'finish'];
  switch(this.currentTool){
    case 'obstacles' :
      if(allowed.indexOf('obstacles')!=-1) this.addObstacle(pos); break;
    case 'erase' :
      if(allowed.indexOf('erase')!=-1) this.erase(pos); break;
    case 'coins' :
      if(allowed.indexOf('coins')!=-1) this.addCoin(pos); break;
    case 'mob' :
      if(allowed.indexOf('mob')!=-1) this.addMob(pos); break;
    case 'checkpnt' :
      if(allowed.indexOf('checkpnt')!=-1) this.addCheckPoint(pos); break;
    case 'start' :
      if(allowed.indexOf('start')!=-1) this.startLocation = pos.copy(); break;
    case 'finish' :
      if(allowed.indexOf('finish')!=-1) this.finishLocation = pos.copy(); break;
  }
};

//draw the cursor for the level editor
LE.prototype.drawCursor = function(){
  //if there is no mouse coordinate, do not draw
  if(!this.hasMouse()) return;
  var changeColor = (color) => this.canvas.setFill(color);

  //get the grid position
  var p = this.getMouseWorldGridCoordinate();

  if(this.currentTool == 'move')
    this.canvas.dimage(this.mouse.x - this.toolIcons.move.w / 2, this.mouse.y - this.toolIcons.move.h / 2, this.toolIcons.move);
  if(this.currentTool == 'erase')
    this.canvas.dimage(this.mouse.x, this.mouse.y - this.toolIcons.erase.h, this.toolIcons.erase);

  this.toWorldCoodinates();

  if(this.currentTool == 'obstacles'){
    changeColor('rgb(44,62,80)');
    this.canvas.rectf(p.x, p.y, 20,20);
  }

  if(this.currentTool == 'start'){
    changeColor('rgb(46,204,113)');
    this.canvas.rectf(p.x, p.y, 20,40);
  }
  if(this.currentTool == 'finish'){
    changeColor('rgb(243,156,18)');
    this.canvas.rectf(p.x, p.y, 20,40);
  }

  if(this.currentTool == 'coins'){
    changeColor('rgb(241,196,15)');
    this.canvas.circlef(p.x+10, p.y+10, 10);
  }
  if(this.currentTool == 'mob'){
    changeColor('rgb(192,57,43)');
    this.canvas.rectf(p.x, p.y, 20, 40);
  }
  if(this.currentTool == 'checkpnt')
    this.canvas.dimage(p.x, p.y, this.toolIcons.checkpoint);


  this.toCanvasCoordinates();
};

//remove an item from the listObject at gridPosition
LE.removeFromList = function(listObject, gridPosition){
  if(LE.existsInList(listObject, gridPosition)){
    delete listObject[gridPosition + ''];
  }
};

//returns whether an item is in a list object @ position
LE.existsInList = function(listObject, position){
  var hash = position + '';
  return listObject[hash] !== undefined;
};

//decomposes the pixel-blocks into larger blocks for game run time
LE.decompose = function(obs){
  var ret = [];
  while(LE.objListToArray(obs).length > 0){
    var lb = LE.findLargestBlock(obs);
    if(lb !== null){
      ret.push(lb);
      LE.removeBlock(lb, obs);
    }else break;
  }
  return ret;
};

//compute some stats given the points of the world (min and max positions aka the size of the level)
LE.stats = function(obs, othersList){
  obs=LE.objListToArray(obs).map(b => b.p);
  othersList=othersList.map(ol => LE.objListToArray(ol));
  var l = othersList.reduce((list, subList) => list.concat(subList), []).concat(obs);
  var min = l[0].copy();
  var max = min.copy();
  l.forEach(p => {
    min.x = Math.min(min.x, p.x);
    min.y = Math.min(min.y, p.y);
    min.z = Math.min(min.z, p.z);
    max.x = Math.max(max.x, p.x);
    max.y = Math.max(max.y, p.y);
    max.z = Math.max(max.z, p.z);
  });
  return {min:min,max:max};
};

LE.objListToArray = function(o){
  var ret = [];
  for(var n in o)
    ret.push(o[n]);
  return ret;
};

//deep copy some object
LE.deepCopyObject = function(o){
  var ret = {};
  for(var n in o)
    ret[n] = o[n].copy();
  return ret;
};

LE.countIterations = function(p, obs, xInc, yInc){
  var count = 0;
  var iter = p.copy();
  for(count = 0; ; count++){
    if(!LE.existsInList(obs, iter)) break;
    iter.x += xInc;
    iter.y += yInc;
  }
  return count;
};

LE.findLargestBlockX = function(p, obs){
  var width = LE.countIterations(p, obs, 20, 0);
  var height = LE.countIterations(p, obs, 0, 20);
  var i;
  for(i = 0; i < height; i++){
    if(LE.countIterations(new LV2(p.x, p.y+i*20), obs, 20, 0) != width) break;
  }
  return new Box(p.x, p.y, width * 20, i*20);
};

LE.findLargestBlockY = function(p, obs){
  var height = LE.countIterations(p, obs, 0, 20);
  var width = LE.countIterations(p, obs, 20, 0);

  var i;
  for(i = 0; i < width; i++){
    if(LE.countIterations(new LV2(p.x+i*20, p.y), obs, 0, 20) != height) break;
  }
  return new Box(p.x, p.y, i * 20, height*20);
};


LE.findLargestBlock = function(obs){
  var p, ret = [];
  for(var n in obs){
    p = obs[n].p;
    ret.push(LE.findLargestBlockX(p, obs));
    ret.push(LE.findLargestBlockY(p, obs));
  }
  ret=ret.filter(b => b.w > 0 && b.h > 0);
  ret=ret.map(b => {
    return Object.assign(b, {area : b.w*b.h +  b.h*500});
  });
  ret.sort((b2, b1) => {
    //if(b2.h <= b1.h) return 100 + (b1.area - b2.area);
    return b1.area - b2.area;
  });
  if(ret.length > 0) return ret[0];
  return null;
};

LE.removeBlock = function(b, obs){
  var p = b.p.copy();
  for(var y = p.y; y < p.y+b.h; y+=20){
    for(var x = p.x; x < p.x+b.w; x+=20){
      LE.removeFromList(obs, new LV2(x,y));
    }
  }
};


//a function to draw a grid to a canvas from a particular origin
LE.drawGrid = function(canvas, origin, color){
  canvas.setStroke(color);
  var y,x;
  var o = new LV2(origin.x / 20, origin.y / 20);
  o.ifloor(); o.iscale(20);
  var W = canvas.w / 2;
  var H = canvas.h / 2;

  for(y = o.y - H; y <= o.y + H; y += 20){
    for(x = o.x - W; x <= o.x + W; x+= 20){
      canvas.rect(x-10,y,20,20);
    }
  }
};

//draw a circle
LE.drawCircle = function(canvas, x, y, r, c){
  canvas.setFill(c);
  canvas.circlef(x,y,r);
};

//a function to draw coins
LE.drawCoins = function(canvas, points){
  for(var n in points){
    var p = points[n];
    LE.drawCircle(canvas, p.x+10, p.y+10, 10, 'rgb(241,196,15)');
  }
};

//a funcction to draw obstacles
LE.drawObstacles = function(canvas, points){
  canvas.setFill('rgb(44,62,80)');
  for(var n in points){
    var o = points[n];
    canvas.rectf(o.p.x, o.p.y, o.w, o.h);
  }
};

LE.drawFigure = function(canvas, point, c){
  canvas.setFill(c || 'white');
  canvas.rectf(point.x, point.y, 20, 40);
};

LE.drawIcons = function(canvas, points, icon){
  for(var n in points){
    var p = points[n];
    canvas.dimage(p.x, p.y, icon);
  }
};

LE.drawMobs = function(canvas, points){
  canvas.setFill('rgb(192,57,43)');
  for(var n in points){
    var p = points[n];
    canvas.rectf(p.x, p.y, 20, 40);
  }
};
